package com.huixian.core.util;

import com.huixian.common2.enums.RequestContextEnum;
import com.huixian.common2.exception.FrameworkException;
import com.huixian.common2.exception.HuixianException;
import com.huixian.common2.util.PropertiesUtil;
import com.huixian.common2.vo.ApiResponse;
import com.huixian.common2.vo.RequestContext;
import org.apache.commons.lang3.StringUtils;

/**
 * ApiResponse 处理工具类
 *
 * @author : polegek
 * @date : Created in 2019-10-21 16:34
 */
public class ApiResponseUtil {

    /**
     * 获取错误码
     * 框架异常若没有组装，且不是特殊错误码，则封装为系统错误
     *
     * @param frameworkException 框架错误异常
     *
     * @return 错误码
     */
    public static Integer getErrorCode(FrameworkException frameworkException) {
        int errorCode = 0;

        // 如果异常不存在，在初始化化默认异常
        if (frameworkException == null) {
            frameworkException = new FrameworkException();
        }

        // 错误码不为0，则已拼接完成，直接返回
        if (frameworkException.getErrorCode() != 0) {
            return frameworkException.getErrorCode();
        }

        return getErrorCode(new HuixianException());
    }

    /**
     * 获取错误码
     *
     * @param huixianException 错误异常
     *
     * @return 错误码
     */
    public static Integer getErrorCode(HuixianException huixianException) {
        int errorCode = 0;

        // 如果异常不存在，在初始化化默认异常
        if (huixianException == null) {
            huixianException = new HuixianException();
        }

        // 错误码不为0，则已拼接完成，直接返回
        if (huixianException.getErrorCode() != 0) {
            return huixianException.getErrorCode();
        }

        // 拼接错误码
        int systemNum = Integer.parseInt(PropertiesUtil.getProperty("com.huixian.system-num"));
        String baseCode = StringUtils.leftPad(String.valueOf(huixianException.getBaseCode()), 3, "0");
        errorCode = Integer
                .parseInt(StringUtils
                        .join(systemNum, huixianException.getLayer(),
                                baseCode));
        return errorCode;
    }

    /**
     * 构建错误响应
     *
     * @param huixianException HuixianException
     *
     * @return 错误响应
     */
    public static ApiResponse errorResponse(HuixianException huixianException) {
        return errorResponse(getErrorCode(huixianException), huixianException.getMessage());
    }

    /**
     * 构建错误响应
     *
     * @return 错误响应
     */
    public static ApiResponse errorResponse() {
        return errorResponse(new HuixianException());
    }

    public static ApiResponse errorResponse(Integer errorCode, String errorStr) {
        String token = RequestContext.getContext().getContextDetail(RequestContextEnum.HX_RETURN_TOKEN.name());
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setErrorCode(errorCode);
        apiResponse.setErrorStr(errorStr);
        apiResponse.setToken(token);
        return apiResponse;
    }
}
