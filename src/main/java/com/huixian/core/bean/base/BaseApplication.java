package com.huixian.core.bean.base;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.huixian.core.filter.HuiXianRequestFilter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;

/**
 * Description: SpringBoot 基础启动类
 *
 * @author : LuShunNeng
 * @date : Created in 2018/9/23 下午6:51
 */
@MapperScan(basePackages = "com.huixian.*.dmr.*.mapper")
public class BaseApplication {

  /**
   * 分页插件
   */
  @Bean
  public PaginationInterceptor paginationInterceptor() {
    return new PaginationInterceptor();
  }

  @Bean
  public FilterRegistrationBean testFilterRegistration() {
    FilterRegistrationBean registration = new FilterRegistrationBean();
    registration.setFilter(new HuiXianRequestFilter());
    registration.addUrlPatterns("/*");
    registration.setName("huiXianRequestFilter");
    registration.setOrder(-100);
    return registration;
  }
}
