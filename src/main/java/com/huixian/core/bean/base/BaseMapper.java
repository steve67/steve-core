package com.huixian.core.bean.base;

import java.io.Serializable;

/**
 * CreateBy KKYV 2018/9/30
 */
public interface BaseMapper<T> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {

    /**
     * 根据主键加锁
     * @param id 主键id
     * @return 实体DO
     */
    T getByPrimaryForUpdate(Serializable id);
}
