package com.huixian.core.bean.base;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.io.Serializable;

/**
 * CreateBy KKYV 2018/9/30
 */
public class BaseRepository<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> {

    protected static final String FOR_UPDATE = "for update";

    protected static final String LIMIT_1 = "limit 1";

    protected String limitN(Integer n) {
        if(n == null) {
            throw new IllegalArgumentException("n不能为空");
        }
        return "limit ".concat(n.toString());
    }
    /**
     * 查询wrapper
     * @return wrapper
     */
    protected LambdaQueryWrapper<T> queryWrapper() {
        return new QueryWrapper<T>().lambda();
    }

    /**
     * 更新wrapper
     * @return
     */
    protected LambdaUpdateWrapper<T> updateWrapper() {
        return new UpdateWrapper<T>().lambda();
    }

    /**
     * 根据主键加锁
     * @param id id
     * @return 实体DO
     */
    protected T getByPrimaryForUpdate(Serializable id) {
        return this.baseMapper.getByPrimaryForUpdate(id);
    }
}
