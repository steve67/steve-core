package com.huixian.core.config;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * Created by snlu on 2018/9/22.
 * 慧贤系统配置类
 */
@Data
@Validated
@ConfigurationProperties(prefix = "com.huixian")
public class HuiXianConfigurer implements Serializable{

  private static final long serialVersionUID = -8083824196026972702L;

  /**
   * 系统号
   */
  @NotBlank
  private String systemNum;

}
