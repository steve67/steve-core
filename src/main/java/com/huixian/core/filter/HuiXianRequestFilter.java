package com.huixian.core.filter;

import com.huixian.common2.enums.RequestContextEnum;
import com.huixian.common2.vo.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.MDC;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Description:   .
 *
 * @author : LuShunNeng
 * @date : Created in 2018/12/19 下午4:29
 */
@Slf4j
public class HuiXianRequestFilter implements Filter {

    /**
     * 统一token前缀
     */
    private static final String TOKEN_PREFIX = "Bearer ";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        Timer requestLatency= PrometheusUtil.timerInit(StringUtils.join(PrometheusTrack.PROMETHEUS_PREFIX,"-http-latency"),"url",request.getRequestURI());

        // traceId
        String traceId = request.getHeader(RequestContextEnum.TRACE_ID.getOutCode());
        MDC.put(RequestContextEnum.TRACE_ID.name(), traceId);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        RequestContext requestContext = RequestContext.getContext();
        if (StringUtils.isBlank(traceId)) {
            traceId = UUID.randomUUID().toString();
        }
        requestContext.addContextDetail(RequestContextEnum.TRACE_ID.name(), traceId);
        log.info("请求url{}:", request.getRequestURI());
        // token
        String token = request.getHeader(RequestContextEnum.HX_TOKEN.getOutCode());
        if (StringUtils.isNotBlank(token)) {
            requestContext.addContextDetail(RequestContextEnum.HX_TOKEN.name(), token.replace(TOKEN_PREFIX, ""));
        }
        log.debug("在请求处理之前生成 logback ", RequestContextEnum.TRACE_ID.name(),
                ":", traceId);

        chain.doFilter(servletRequest, response);

        stopWatch.stop();
//        requestLatency.record(stopWatch.getTime(TimeUnit.MILLISECONDS),TimeUnit.MILLISECONDS);
        log.info("请求耗时：{}ms", stopWatch.getTime(TimeUnit.MILLISECONDS));
        log.debug("整个请求处理完毕清除 logback MDC");
        MDC.clear();
        RequestContext.clear();
    }

    @Override
    public void destroy() {

    }
}
