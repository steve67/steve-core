package com.huixian.core.aop;

import com.huixian.common2.exception.BusinessException;
import com.huixian.common2.exception.FrameworkException;
import com.huixian.common2.exception.HuixianException;
import com.huixian.common2.model.BizException;
import com.huixian.common2.model.CommonError;
import com.huixian.common2.model.SystemException;
import com.huixian.common2.util.ApiResponseUtil;
import com.huixian.common2.vo.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.skywalking.apm.toolkit.trace.ActiveSpan;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * create at  2018/09/05 10:47
 *
 * @author yanggang
 */
@RestControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    /**
     * 自定义参数校验异常处理
     *
     * @param exception .
     *
     * @return .
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ApiResponse methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException exception) {
        log.info("自定义参数异常[WARN]: {}", exception.getMessage());
        log.warn("自定义参数异常: ", exception);
        ApiResponse apiResponse =  ApiResponseUtil.errorResponse(CommonError.PARAM_ERROR,
                Objects.requireNonNull(exception.getBindingResult().getFieldError()).getDefaultMessage());
        errorCodeCount(apiResponse.getErrorCode());
        return apiResponse;
    }

    /**
     * 参数校验异常
     *
     * @param exception 绑定校验异常
     *
     * @return .
     */
    @ExceptionHandler({BindException.class})
    public ApiResponse bindExceptionHandler(BindException exception) {
        log.info("参数异常[WARN]: {}", exception.getMessage());
        log.warn("参数异常: ", exception);
        ApiResponse apiResponse =  ApiResponseUtil.errorResponse(CommonError.PARAM_ERROR,
                Objects.requireNonNull(exception.getBindingResult().getFieldError()).getDefaultMessage());
        errorCodeCount(apiResponse.getErrorCode());
        return apiResponse;
    }


    @ExceptionHandler({ConstraintViolationException.class})
    public ApiResponse constraintViolationExceptionHandler(ConstraintViolationException exception) {
        log.info("自定义参数异常[WARN]: {}", exception.getMessage());
        log.warn("自定义参数异常: ", exception);
        final List<String> errorList = new ArrayList<>(exception.getConstraintViolations().size());
        exception.getConstraintViolations().forEach(item -> {
            errorList.add(item.getMessageTemplate());
        });
        ApiResponse apiResponse = ApiResponseUtil.errorResponse(CommonError.PARAM_ERROR, StringUtils.join(errorList,"|"));
        errorCodeCount(apiResponse.getErrorCode());
        return apiResponse;
    }

    /**
     * SystemException异常
     *
     * @param exception .
     *
     * @return .
     */
    @Deprecated
    @ExceptionHandler({SystemException.class})
    public ApiResponse systemExceptionHandler(SystemException exception) {
        log.info("SystemException异常[ERROR]: {}", exception.getMessage());
        log.error("SystemException异常: ", exception);
        ApiResponse apiResponse;
        if (exception.getErrorCode() != null) {
            apiResponse = ApiResponseUtil.errorResponse(exception.getErrorCode(),exception.getMessage());
        }else {
            apiResponse = ApiResponseUtil.errorResponse(exception.getError(),exception.getMessage());
        }
        errorCodeCount(apiResponse.getErrorCode());
        return apiResponse;
    }

    /**
     * BizException异常
     *
     * @param exception .
     *
     * @return .
     */
    @Deprecated
    @ExceptionHandler({BizException.class})
    public ApiResponse bizExceptionHandler(BizException exception) {
        log.info("BizException异常[WARN]: {}", exception.getMessage());
        log.warn("BizException异常: ", exception);
        ApiResponse apiResponse;
        if (exception.getErrorCode() != null) {
            apiResponse =  ApiResponseUtil.errorResponse(exception.getErrorCode(),exception.getMessage());
        }else {
            apiResponse = ApiResponseUtil.errorResponse(exception.getError(),exception.getMessage());
        }
        errorCodeCount(apiResponse.getErrorCode());
        return apiResponse;
    }

    /**
     * huixianException 异常
     *
     * @param frameworkException 慧贤框架异常
     *
     * @return Api Json格式
     */
    @ExceptionHandler({FrameworkException.class})
    public ApiResponse exception(FrameworkException frameworkException) {
        log.info(frameworkException.getClass().getName() + " 异常[WARN]: {}", frameworkException.getMessage());
        log.warn(frameworkException.getClass().getName() + " 异常: ", frameworkException);

        ApiResponse apiResponse = com.huixian.core.util.ApiResponseUtil
                .errorResponse(frameworkException);

        errorCodeCount(apiResponse.getErrorCode());
        return apiResponse;
    }

    /**
     * huixianException 异常
     *
     * @param huixianException 慧贤异常
     *
     * @return Api Json格式
     */
    @ExceptionHandler({HuixianException.class, BusinessException.class})
    public ApiResponse exception(HuixianException huixianException) {
        log.info(huixianException.getClass().getName() + " 异常[WARN]: {}", huixianException.getMessage());
        log.warn(huixianException.getClass().getName() + " 异常: ", huixianException);

        ApiResponse apiResponse = com.huixian.core.util.ApiResponseUtil
                .errorResponse(huixianException);

        errorCodeCount(apiResponse.getErrorCode());
        return apiResponse;
    }

    /**
     * 全局异常
     *
     * @param exception .
     *
     * @return .
     */
    @ExceptionHandler({Exception.class})
    @Trace
    public ApiResponse exceptionHandler(Exception exception) {
        log.info("全局异常[ERROR]: {}", exception.getMessage());
        log.error("全局异常: ", exception);
        ActiveSpan.error(exception);
        ApiResponse apiResponse = com.huixian.core.util.ApiResponseUtil.errorResponse();
        errorCodeCount(apiResponse.getErrorCode());
        return apiResponse;
    }

    /**
     * 错误code统计
     * @param errorCode
     */
    public void errorCodeCount(Integer errorCode){
//        Counter counter= PrometheusUtil.countInit(PrometheusTrack.PROMETHEUS_PREFIX+"-error-code","error-code",errorCode.toString());
//        counter.increment();
    }

}
