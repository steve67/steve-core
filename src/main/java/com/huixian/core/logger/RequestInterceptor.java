package com.huixian.core.logger;

import com.huixian.common2.enums.RequestContextEnum;
import com.huixian.common2.vo.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * Created by snlu on 2018/8/20.
 */
@Slf4j
public class RequestInterceptor extends HandlerInterceptorAdapter {

  /**
   * 在请求处理之前回调方法
   */
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    RequestContext requestContext = RequestContext.getContext();
    // traceId
    String traceId = request.getHeader(RequestContextEnum.TRACE_ID.getOutCode());
    if (StringUtils.isBlank(traceId)) {
      traceId = UUID.randomUUID().toString();
    }
    requestContext.addContextDetail(RequestContextEnum.TRACE_ID.name(), traceId);

    // token
    String token = request.getHeader(RequestContextEnum.HX_TOKEN.getOutCode());
    if (StringUtils.isNotBlank(token)) {
      requestContext.addContextDetail(RequestContextEnum.HX_TOKEN.name(), token);
    }
    MDC.put(RequestContextEnum.TRACE_ID.name(), traceId);
    log.debug("在请求处理之前生成 logback ", RequestContextEnum.TRACE_ID.name(),
        ":", traceId);
    // 只有返回true才会继续向下执行，返回false取消当前请求
    return true;
  }

  /**
   * 请求处理之后回调方法
   */
  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
      ModelAndView modelAndView) throws Exception {
    // 线程结束后需要清除,否则当前线程会一直占用这个requestId值
    log.debug("请求处理之后可操作 logback MDC");
  }

  /**
   * 整个请求处理完毕回调方法
   */
  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
      Object handler, Exception ex) throws Exception {
    // 整个请求线程结束后需要清除,否则当前线程会一直占用这个requestId值
    log.debug("整个请求处理完毕清除 logback MDC");
    MDC.clear();
    RequestContext.clear();
  }
}
