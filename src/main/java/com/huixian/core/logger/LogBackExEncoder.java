package com.huixian.core.logger;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import org.apache.skywalking.apm.toolkit.log.logback.v1.x.LogbackPatternConverter;

/**
 * Created by snlu on 2018/8/20.
 */
public class LogBackExEncoder extends PatternLayoutEncoder {
  static {
    PatternLayout.defaultConverterMap.put("T", ThreadNumConverter.class.getName());
    // skywalking的traceId
    PatternLayout.defaultConverterMap.put("tid", LogbackPatternConverter.class.getName());
  }
}
